package com.emiratesauction.challenge.base.presentation.view.contract;

public interface MvpContract {

    interface View {
        Presenter getPresenter();
    }

    interface Presenter<T extends View> {
        T getView();

        void onAttach(T view);

        void onDetach();

        void clearDisposables();
    }
}