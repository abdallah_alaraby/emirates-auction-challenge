package com.emiratesauction.challenge.base.data.cloud;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class CloudManager {
    private static final String baseUrl = "http://api.emiratesauction.com/v2/";
    private static final CloudManager INSTANCE = new CloudManager();

    private CloudManager() {
    }

    public static CloudManager getInstance() {
        return INSTANCE;
    }

    /**
     * Initializes a service interface, and adds the initial configurations for network calls
     * such as call adapter factories, and the base url
     */
    public <T> T requestApi(Class<T> service) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(getClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(service);
    }

    /**
     * @return OkHttpClient that comes with a logging intercepter which logs network requests and responses
     */
    private OkHttpClient getClient() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .build();
    }
}