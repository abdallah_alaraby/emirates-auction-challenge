package com.emiratesauction.challenge.base.presentation.util;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.widget.ImageView;

public class ImageLoader {

    public static void loadImage(Context context, String imageUrl, @DrawableRes int placeHolder,
                                 @DrawableRes int errorDrawable, ImageView targetImageView) {
        targetImageView.post(() ->
                GlideApp.with(context)
                        .load(imageUrl)
                        .placeholder(placeHolder)
                        .error(errorDrawable)
                        .into(targetImageView));
    }
}