package com.emiratesauction.challenge.base.presentation.util;

import android.content.Context;
import android.support.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.Registry;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.module.AppGlideModule;

@GlideModule
public class AuctionGlideModule extends AppGlideModule {
    private static final int CACHE_DISK_SIZE_IN_B = 100 * 1024 * 1024; //100MB

    @Override
    public void applyOptions(Context context, GlideBuilder builder) {

        builder.setDiskCache(
                new InternalCacheDiskCacheFactory(context, CACHE_DISK_SIZE_IN_B));
    }

    @Override
    public void registerComponents(@NonNull Context context, @NonNull Glide glide, @NonNull Registry registry) {
        super.registerComponents(context, glide, registry);
    }
}