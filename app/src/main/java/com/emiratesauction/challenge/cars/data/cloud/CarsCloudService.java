package com.emiratesauction.challenge.cars.data.cloud;

import com.emiratesauction.challenge.cars.data.model.CarsAuctionResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CarsCloudService {
    @GET("carsonline")
    Observable<CarsAuctionResponse> getCarsAuction(@Query("Ticks") String ticks);
}