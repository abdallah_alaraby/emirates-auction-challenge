package com.emiratesauction.challenge.cars.presentation.view.presenter;

import com.emiratesauction.challenge.cars.data.cloud.CarsCloudManager;
import com.emiratesauction.challenge.cars.data.model.Auction;
import com.emiratesauction.challenge.cars.presentation.view.contract.AuctionsContract;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CarsPresenter implements AuctionsContract.Presenter {
    private Disposable mLoadCarsDisposable;
    private Disposable mScheduleLoadCarsDisposable;
    private WeakReference<AuctionsContract.View> mViewRef;
    private CarsCloudManager mCarsCloudManager = new CarsCloudManager();

    @Override
    public AuctionsContract.View getView() {
        return mViewRef.get();
    }

    @Override
    public void onAttach(AuctionsContract.View view) {
        mViewRef = new WeakReference<>(view);
    }

    @Override
    public void onDetach() {
        if (mViewRef != null) {
            mViewRef.clear();
            mViewRef = null;
        }

        clearDisposables();
    }

    @Override
    public void clearDisposables() {
        if (mLoadCarsDisposable != null
                && !mLoadCarsDisposable.isDisposed()) {
            mLoadCarsDisposable.dispose();
        }
        if (mScheduleLoadCarsDisposable != null
                && !mScheduleLoadCarsDisposable.isDisposed()) {
            mScheduleLoadCarsDisposable.dispose();
        }
    }

    @Override
    public void loadCars(int page, int limit, String ticks) {
        if (mLoadCarsDisposable != null && !mLoadCarsDisposable.isDisposed()) {
            mLoadCarsDisposable.dispose();
        }
        mLoadCarsDisposable = mCarsCloudManager
                .getCarsAuction(ticks)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(carsAuctionResponse -> {
                    boolean isUpdating = ticks != null;
                    List<Auction> cars;
                    if (isUpdating) {
                        cars = carsAuctionResponse.getCars();
                    } else {
                        int startIndex = (page - 1) * limit;
                        int endIndex = (page * limit);
                        if (endIndex >= carsAuctionResponse.getCars().size()) {
                            endIndex = carsAuctionResponse.getCars().size();
                        }
                        cars = carsAuctionResponse.getCars().subList(startIndex, endIndex);
                    }
                    if (page == 1) {
                        scheduleRefresh(carsAuctionResponse.getRefreshInterval(), carsAuctionResponse.getTicks());
                    }
                    getView().setLoading(false);
                    getView().onLoadCarsPageSuccess(cars, isUpdating);
                }, throwable -> {
                    getView().setLoading(false);
                    if (throwable instanceof IOException) {
                        getView().showNetworkError();
                    } else {
                        getView().showUnknownError();
                    }
                });
    }

    @Override
    public void scheduleRefresh(int refreshInterval, String ticks) {
        if (mScheduleLoadCarsDisposable != null && !mScheduleLoadCarsDisposable.isDisposed()) {
            mScheduleLoadCarsDisposable.dispose();
        }

        mScheduleLoadCarsDisposable =
                Observable.interval(1, TimeUnit.SECONDS)
                        .takeUntil(aLong -> aLong >= refreshInterval)
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnComplete(() -> getView().refresh(ticks))
                        .subscribe();
    }
}