package com.emiratesauction.challenge.cars.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Error {
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("en")
    @Expose
    private String en;
    @SerializedName("ar")
    @Expose
    private String ar;

    public String getCode() {
        return code;
    }

    public String getEn() {
        return en;
    }

    public String getAr() {
        return ar;
    }
}
