package com.emiratesauction.challenge.cars.data.cloud;

import com.emiratesauction.challenge.base.data.cloud.CloudManager;
import com.emiratesauction.challenge.cars.data.model.CarsAuctionResponse;

import io.reactivex.Observable;

public class CarsCloudManager {

    public Observable<CarsAuctionResponse> getCarsAuction(String ticks) {
        return CloudManager
                .getInstance()
                .requestApi(CarsCloudService.class)
                .getCarsAuction(ticks);
    }
}