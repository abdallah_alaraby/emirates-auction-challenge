package com.emiratesauction.challenge.cars.presentation.view.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emiratesauction.challenge.R;
import com.emiratesauction.challenge.cars.data.model.Auction;
import com.emiratesauction.challenge.cars.presentation.view.adapter.AuctionsAdapter;
import com.emiratesauction.challenge.cars.presentation.view.contract.AuctionsContract;
import com.emiratesauction.challenge.cars.presentation.view.presenter.CarsPresenter;

import java.util.List;

public class AuctionsFragment extends Fragment implements AuctionsContract.View,
        SwipeRefreshLayout.OnRefreshListener {
    private final int mLimit = 10;
    private final int mVisibleThreshold = 5;
    private AuctionsContract.Presenter mPresenter;
    private SwipeRefreshLayout swipeRefreshCars;
    private RecyclerView recyclerCars;
    private SwipeRefreshLayout swipeRefreshError;
    private TextView textError;
    private AuctionsAdapter mAuctionsAdapter;
    private int mPage = 1;
    private boolean mIsLoadingMore = false;
    private int mSelectedSort = -1;
    boolean mIsAscending = true;

    public static AuctionsFragment newInstance() {
        return new AuctionsFragment();
    }

    @Override
    public AuctionsContract.Presenter getPresenter() {
        initPresenter();
        return mPresenter;
    }

    private void initPresenter() {
        if (mPresenter == null) {
            mPresenter = new CarsPresenter();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cars, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAuctionsAdapter = new AuctionsAdapter();
        initViews(view);
        refresh(null);
    }

    private void initViews(View view) {
        swipeRefreshCars = view.findViewById(R.id.swipeRefreshCars);
        swipeRefreshError = view.findViewById(R.id.swipeRefreshError);
        textError = view.findViewById(R.id.textError);
        recyclerCars = view.findViewById(R.id.recyclerCars);
        recyclerCars.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerCars.setAdapter(mAuctionsAdapter);
        swipeRefreshCars.setOnRefreshListener(this);
        swipeRefreshError.setOnRefreshListener(this);
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerCars
                .getLayoutManager();
        recyclerCars
                .addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView,
                                           int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);

                        if (!swipeRefreshCars.isRefreshing()
                                && !swipeRefreshError.isRefreshing()) {
                            int totalItemCount = linearLayoutManager.getItemCount();
                            int lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            if (!mIsLoadingMore
                                    && totalItemCount <= (lastVisibleItem + mVisibleThreshold)) {
                                loadNextPage(null);
                                mAuctionsAdapter.addLoading();
                                mIsLoadingMore = true;
                            }
                        }
                    }
                });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        getPresenter().onAttach(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        getPresenter().onAttach(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getPresenter().onDetach();
    }

    @Override
    public void onLoadCarsPageSuccess(List<Auction> auctions, boolean isUpdating) {
        mAuctionsAdapter.removeLoading();
        if (isUpdating) {
            mAuctionsAdapter.updateAuctions(auctions);
        } else {
            if (mPage == 1) {
                mAuctionsAdapter.clearItems();
            }
            mAuctionsAdapter.addItems(auctions);
            mAuctionsAdapter.notifyDataSetChanged();
            mPage++;
        }
    }

    @Override
    public void showNetworkError() {
        showErrorMsg(getString(R.string.error_network));
    }

    @Override
    public void showUnknownError() {
        showErrorMsg(getString(R.string.error_unknown));
    }

    @Override
    public void showErrorMsg(String errorMsg) {
        swipeRefreshCars.setVisibility(View.GONE);
        swipeRefreshError.setVisibility(View.VISIBLE);
        textError.setText(errorMsg);
    }

    @Override
    public void setLoading(boolean isLoading) {
        swipeRefreshCars.setRefreshing(isLoading);
        swipeRefreshError.setRefreshing(isLoading);
        mIsLoadingMore = false;
    }

    @Override
    public void onRefresh() {
        refresh(null);
    }

    @Override
    public void refresh(String ticks) {
        setLoading(true);
        mPage = 1;
        loadNextPage(ticks);
    }

    private void loadNextPage(String ticks) {
        getPresenter().loadCars(mPage, mLimit, ticks);
    }

    public void showSortDialog() {
        new AlertDialog.Builder(getActivity())
                .setItems(R.array.sort_cars,
                        (dialog, which) -> {
                            mIsAscending = mSelectedSort != which || !mIsAscending;
                            switch (which) {
                                case 0:
                                    mAuctionsAdapter.sortByEndDate(mIsAscending);
                                    break;
                                case 1:
                                    mAuctionsAdapter.sortByPrice(mIsAscending);
                                    break;
                                case 2:
                                    mAuctionsAdapter.sortByYear(mIsAscending);
                                    break;
                            }
                            mSelectedSort = which;
                        }).show();
    }
}