package com.emiratesauction.challenge.cars.presentation.view.activity;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.emiratesauction.challenge.R;
import com.emiratesauction.challenge.cars.presentation.view.fragment.AuctionsFragment;

public class CarsActivity extends AppCompatActivity {
    private AuctionsFragment mAuctionsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cars);
        mAuctionsFragment = AuctionsFragment.newInstance();
        setFragment(mAuctionsFragment);
        findViewById(R.id.frame_sort).setOnClickListener(v -> {
            if (mAuctionsFragment != null) {
                mAuctionsFragment.showSortDialog();
            }
        });
    }

    void setFragment(Fragment fragment) {
        FragmentTransaction tr = getFragmentManager().beginTransaction();
        tr.replace(R.id.frameFragmentContainer, fragment, fragment.getClass().getSimpleName());
        tr.commit();
    }
}