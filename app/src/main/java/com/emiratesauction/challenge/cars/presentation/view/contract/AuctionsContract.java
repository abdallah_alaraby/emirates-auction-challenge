package com.emiratesauction.challenge.cars.presentation.view.contract;

import com.emiratesauction.challenge.base.presentation.view.contract.MvpContract;
import com.emiratesauction.challenge.cars.data.model.Auction;

import java.util.List;

public interface AuctionsContract {

    interface View extends MvpContract.View {

        void onLoadCarsPageSuccess(List<Auction> auctions, boolean isUpdating);

        void showNetworkError();

        void showUnknownError();

        void showErrorMsg(String errorMsg);

        void setLoading(boolean isLoading);

        void refresh(String ticks);
    }

    interface Presenter extends MvpContract.Presenter<View> {
        void loadCars(int page, int limit, String ticks);

        void scheduleRefresh(int refreshInterval, String ticks);
    }
}