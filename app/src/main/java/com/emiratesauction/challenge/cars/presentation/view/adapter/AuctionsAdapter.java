package com.emiratesauction.challenge.cars.presentation.view.adapter;

import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.emiratesauction.challenge.R;
import com.emiratesauction.challenge.base.presentation.util.ImageLoader;
import com.emiratesauction.challenge.cars.data.model.Auction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class AuctionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_AUCTION = 1;
    private static final int VIEW_TYPE_LOADING = 2;
    private List<Auction> mAuctions = new ArrayList<>();

    public void addItems(List<Auction> auctions) {
        mAuctions.addAll(auctions);
    }

    public void addLoading() {
        mAuctions.add(null);
    }

    public void removeLoading() {
        if (mAuctions.size() > 0 && mAuctions.get(mAuctions.size() - 1) == null) {
            mAuctions.remove(mAuctions.size() - 1);
        }
    }

    public void clearItems() {
        mAuctions.clear();
    }

    public void updateAuctions(List<Auction> auctions) {
        for (Auction auction : auctions) {
            int index = mAuctions.indexOf(auction);
            if (index != -1) {
                mAuctions.set(index, auction);
                notifyItemChanged(index);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mAuctions.get(position) != null ? VIEW_TYPE_AUCTION : VIEW_TYPE_LOADING;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_AUCTION) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_auction, parent, false);
            return new AuctionViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder vh, int position) {
        if (vh instanceof AuctionViewHolder) {
            AuctionViewHolder holder = (AuctionViewHolder) vh;
            Auction auction = mAuctions.get(position);
            ImageLoader.loadImage(holder.imageItem.getContext(),
                    auction.getImage().replace("[w]", "500").replace("[h]", "500"),
                    R.drawable.auction_placeholder,
                    R.drawable.auction_placeholder,
                    holder.imageItem);
            boolean isArabic = Locale.getDefault().getLanguage().equals("ar");
            holder.textItemName.setText(getTitle(auction));
            holder.textItemPrice.setText(String.valueOf(auction.getAuctionInfo().getCurrentPrice()));
            holder.textItemCurrency.setText(isArabic ?
                    auction.getAuctionInfo().getCurrencyAr() :
                    auction.getAuctionInfo().getCurrencyEn());
            holder.textLotNumber.setText(String.valueOf(auction.getAuctionInfo().getLot()));
            holder.textBids.setText(String.valueOf(auction.getAuctionInfo().getBids()));
            if (holder.countDownTimer != null) {
                holder.countDownTimer.cancel();
            }
            holder.countDownTimer = new CountDownTimer(auction.getAuctionInfo().getEndDate() * 1000, 1000L) {

                @Override
                public void onTick(long millisUntilFinished) {
                    long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished) % 24;
                    long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60;
                    long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60;
                    String text = String.format(Locale.getDefault(), "%02d:%02d:%02d",
                            hours,
                            minutes,
                            seconds);
                    holder.textTimeLeft.setText(text);
                    holder.textTimeLeft.setTextColor(
                            ContextCompat.getColor(holder.textTimeLeft.getContext(),
                                    minutes > 5 ? R.color.black : R.color.red));
                }

                @Override
                public void onFinish() {

                }
            }.start();
        }
    }

    private String getTitle(Auction auction) {
        boolean isArabic = Locale.getDefault().getLanguage().equals("ar");
        return (isArabic ? auction.getMakeAr() : auction.getMakeEn()) +
                " " +
                (isArabic ? auction.getModelAr() : auction.getModelEn()) +
                " " +
                String.valueOf(auction.getYear());
    }

    @Override
    public int getItemCount() {
        return mAuctions.size();
    }

    public void sortByEndDate(boolean mIsAscending) {
        Collections.sort(mAuctions,
                (o1, o2) -> {
                    if (mIsAscending) {
                        return o1.getAuctionInfo().getEndDate() - o2.getAuctionInfo().getEndDate();
                    } else {
                        return o2.getAuctionInfo().getEndDate() - o1.getAuctionInfo().getEndDate();
                    }
                });
        notifyDataSetChanged();
    }

    public void sortByPrice(boolean mIsAscending) {
        Collections.sort(mAuctions,
                (o1, o2) -> {
                    if (mIsAscending) {
                        return o1.getAuctionInfo().getCurrentPrice() - o2.getAuctionInfo().getCurrentPrice();
                    } else {
                        return o2.getAuctionInfo().getCurrentPrice() - o1.getAuctionInfo().getCurrentPrice();
                    }
                });
        notifyDataSetChanged();
    }

    public void sortByYear(boolean mIsAscending) {
        Collections.sort(mAuctions,
                (o1, o2) -> {
                    if (mIsAscending) {
                        return o1.getYear() - o2.getYear();
                    } else {
                        return o2.getYear() - o1.getYear();
                    }
                });
        notifyDataSetChanged();
    }

    static class AuctionViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageItem;
        private TextView textItemName;
        private TextView textItemPrice;
        private TextView textItemCurrency;
        private TextView textLotNumber;
        private TextView textBids;
        private TextView textTimeLeft;
        private CountDownTimer countDownTimer;

        AuctionViewHolder(View itemView) {
            super(itemView);
            imageItem = itemView.findViewById(R.id.imageItem);
            textItemName = itemView.findViewById(R.id.textItemName);
            textItemPrice = itemView.findViewById(R.id.textItemPrice);
            textItemCurrency = itemView.findViewById(R.id.textItemCurrency);
            textLotNumber = itemView.findViewById(R.id.textLotNumber);
            textBids = itemView.findViewById(R.id.textBids);
            textTimeLeft = itemView.findViewById(R.id.textTimeLeft);
        }
    }

    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        LoadingViewHolder(View itemView) {
            super(itemView);
        }
    }
}