# EA Android Coding Challenge
----
By Abdallah Alaraby (abdallah.alaraby@gmail.com)

## Architecture & Structure

In this challenge, the architecture mainly contains two layers `data` and `presentation`

The `data` layer is responsible of making the network operations, and parsing data that is returned from the APIs.

The `presentation` layer is responsible of getting data from the `data` layer, show this data to the user, and handling user interaction with the views. In this layer I'm mainly using MVP pattern to separate dealing with the views from the data layer calls and any non-view operations.

You can think of this architecture as a modified Clean Code Architecture, I haven't used it just for the sake of simplicity and to avoid over-organizing the code.

This architecture is useful for separation of concerns, readability, and unit testing (although not applied)

----
## 3rd Party
I have used the following libraries:

* RxJava2 - to add reactive extensions and make handling the code and threading much easier
* Retrofit2 - To handle network calls, along with RxJava2 Adapter to return `Observable` instead of `Call` objects
* Gson - to parse JSON to POJOs
* Glide - To handle image loading and caching.